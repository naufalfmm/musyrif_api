# API Musyrif
API untuk aplikasi Musyrif. Dikembangkan dengan bahasa Go. Database menggunakan MongoDB dengan library mgo. Aplikasi ini diperlombakan pada MTQMN XV pada cabang lomba Desain Aplikasi Komputer Alquran (DAQ).

Cara Menggunakan:
1. Install MongoDB dan Go terlebih dahulu
2. Run database melalui cmd/terminal dengan mengetikkan mongod --dbpath "path-letak-musyrif_api/db.
   Jika mengalami kegagalan, silahkan baca bagian <b>Import Database</b>
3. Run router.go dengan cara mengetikkan go run router.go pada cmd/terminal
4. Port yang digunakan adalah 10317

Anggota Kelompok (AHA Labs)
1. Dony Rahmad A S (https://gitlab.com/dony_ras) ~ Project Leader & Frontend Developer
2. Kurnia Saputra (https://gitlab.com/krnsptr) ~ Frontend Developer
3. Muhammad Naufal F M (https://gitlab.com/naufalfmm) ~ Backend Developer & Database Engineer

# Import Database
Cara mengimport database ke dalam database MongoDB. Pada tutorial ini, asumsi bahwa MongoDB telah terinstall
1. Hapus file dan folder pada folder "path-letak-musyrif_api"/db
2. Jalankan command mongod --dbpath "path-letak-musyrif_api/db melalui cmd/terminal.
3. Jalankan melalui halaman cmd/terminal lain command mongorestore -d musyrif "path-letak-musyrif_api"