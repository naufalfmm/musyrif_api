package auth

import (
	"net/http"
	"regexp"

	"../konst"
	"../models/jwt"
	"../models/session"
)

func CheckEmail(email string) bool {
	valid,_ := regexp.MatchString("([\\w\\d\\.]+)@(?:([\\w\\d]+))((\\.[a-zA-Z]{2,4}){1,2}$)",email)
	return valid
}

func CheckAuthorization(r *http.Request, ss *session.Session) (bool, string) {
	var jwttoken jwt.JWTMusyrif

	token := r.Header.Get(konst.HeaderToken)
	session := r.Header.Get(konst.HeaderSession)

	if token == "" || session == "" {
		return false, "Session dan Token Tidak Valid"
	}

	//Cek kevalidan token
	jwttoken.SetToken(token)
	if stat, msg := jwttoken.CheckTokenValidity(); !stat {
		return stat, msg
	}

	//Cek kevalidan session
	if !ss.CheckSessionExist(session) {
		return false, "Anda Belum Login"
	}

	return true, ""
}
