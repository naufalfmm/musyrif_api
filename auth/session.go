package auth

import (
	"../models/session"

	"encoding/json"

	"../models/user"

	"../models/enkripsi"
)

// func (s *session.Session) CreateSession(s *mgo.Session, u User) string {
// 	stat,
// }

func LoginSession(s *session.Session, user user.User) (bool, string) {
	var klien session.Client
	var encry enkripsi.Encry

	_, logType := user.GetLoginType()
	klien.CreateClient(user.GetId(), user.GetUsername(), logType)
	klienjson, _ := json.Marshal(klien)

	encry.SetMessageText(klienjson)
	encry.DESEncryption()
	sessid := encry.GetEncryptText()

	if stat := s.CheckSessionExist(sessid); stat {
		return false, sessid
	}

	s.StoreSession(sessid)
	return true, sessid
}

func LogoutSession(s *session.Session, sessid string) (bool, string) {
	if stat := s.CheckSessionExist(sessid); !stat {
		return false, "Session Gagal Dihapus"
	}
	s.DeleteSession(sessid)
	return true, "Session Berhasil Dihapus"
}
