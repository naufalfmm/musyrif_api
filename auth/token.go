package auth

import (
	"../models/jwt"
	"../models/user"
)

func LoginToken(user user.User) string {
	var token jwt.JWTMusyrif

	token.SetMessage(user.GetId())
	token.TokenMaker()
	return token.GetToken()
}
