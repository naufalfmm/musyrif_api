package correctionn

import (
	"encoding/json"
	"net/http"
	"strings"

	"../../models/correction"
	"../../models/session"

	"../hafalan"

	"../../auth"
	"../../konst"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//Format JSON pengiriman:{"koordinat":[{"nomorurut":,"koordinatx":,"koordinaty":},...],"tekskoreksi":}
func SetCorrection(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session, idhafalan string) string {
	var corr correction.Correction

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	ses := s.Copy()
	defer ses.Close()

	err := json.NewDecoder(r.Body).Decode(&corr)
	if err != nil {
		return konst.ErrorReturn(w, "Login Gagal", http.StatusBadRequest)
	}

	corr.SetId(bson.NewObjectId().Hex())
	stat, msg = hafalann.InsertCorrectionToHafalan(ses, w, r, ss, idhafalan, corr)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusBadRequest)
	}
	return konst.SuccessReturn(w, "Berhasil Menambahkan Koreksi", http.StatusOK)
}

func CorrectionController(w http.ResponseWriter, r *http.Request, urle string, s *session.Session) string {
	urle = urle[1:]
	pathe := strings.Split(urle, "/")

	ses, err := mgo.Dial("localhost:27017")
	if err != nil {
		konst.ErrorReturn(w, "Database Down", http.StatusBadGateway)
	}
	defer ses.Close()
	ses.SetMode(mgo.Monotonic, true)

	if pathe[0] == "koreksi" && pathe[1] != "" {
		return SetCorrection(ses, w, r, s, pathe[1])
	}
	return konst.ErrorReturn(w, "Path Tidak Ditemukan", http.StatusNotFound)
}
