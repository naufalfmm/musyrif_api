package hafalann

import (
	"io"
	"net/http"
	"strings"

	"../../models/correction"
	"../../models/hafalan"
	"../../models/jwt"
	"../../models/santri"
	"../../models/session"
	"../../models/user"
	"../../models/ustaz"

	"../../auth"
	"../../konst"

	"encoding/json"

	"os"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func GetHafalanByUser(s *mgo.Session, w http.ResponseWriter, r *http.Request, username string, ss *session.Session) string {
	var haf []hafalan.Hafalan
	var jwtt jwt.JWTMusyrif
	var use user.User

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	ses := s.Copy()
	defer ses.Close()
	d := ses.DB(konst.DBName).C(konst.DBUser)

	token := r.Header.Get(konst.HeaderToken)
	jwtt.SetToken(token)
	jwtt.GetMessageFromToken()
	iduser := jwtt.GetMessage()

	err := d.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&use)
	if err != nil {
		return konst.ErrorReturn(w, "Data User Tidak Ditemukan", http.StatusBadRequest)
	}

	if use.GetUsername() != username {
		return konst.ErrorReturn(w, "You're Not Authorized", http.StatusForbidden)
	}
	c := ses.DB(konst.DBName).C(konst.DBHafalan)

	err = c.Find(bson.M{"username": username}).All(&haf)
	if err != nil {
		return konst.ErrorReturn(w, "Data Hafalan Tidak Ditemukan", http.StatusBadRequest)
	}

	jsonHafalan, erro := json.Marshal(haf)
	if erro != nil {
		return konst.ErrorReturn(w, "Gagal Membuat JSON Hafalan", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	return string(jsonHafalan)
}

//Format json {"namagambar":[1,2,...]}
func UploadHafalan(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session) string {
	var haf hafalan.Hafalan
	var jwtt jwt.JWTMusyrif
	var san santri.Santri
	var dataHafalan, jsonHafalan map[string]interface{}

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	token := r.Header.Get(konst.HeaderToken)
	jwtt.SetToken(token)
	jwtt.GetMessageFromToken()
	iduser := jwtt.GetMessage()

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBSantri)
	err := c.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&san)
	if err != nil {
		return konst.ErrorReturn(w, "Data Santri Tidak Ditemukan", http.StatusBadRequest)
	}

	jsonn := r.FormValue("jsonhafalan")
	err = json.Unmarshal([]byte(jsonn), jsonHafalan)
	if err != nil {
		return konst.ErrorReturn(w, "Format JSON Hafalan Salah", http.StatusBadRequest)
	}

	file, header, err := r.FormFile("hafalan")
	if err != nil {
		return konst.ErrorReturn(w, "Format Request Salah", http.StatusBadRequest)
	}
	defer file.Close()

	formatfile := header.Header.Get("Content-Type")
	if formatfile[:6] != "audio/" {
		return konst.ErrorReturn(w, "Format File Tidak Diterima", http.StatusForbidden)
	}

	dataHafalan["id"] = bson.NewObjectId().Hex()
	dataHafalan["usernamesantri"] = san.GetUsername()
	if san.GetUsernameUstaz() == "" {
		return konst.ErrorReturn(w, "Tidak Ada Data Ustaz", http.StatusForbidden)
	}
	dataHafalan["usernameustaz"] = san.GetUsernameUstaz()
	dataHafalan["namahafalan"] = dataHafalan["id"].(string) + header.Filename[len(header.Filename)-4:]
	dataHafalan["namagambar"] = jsonHafalan["namagambar"].([]float64)

	haf.CreateHafalan(dataHafalan)

	if _, err := os.Stat(konst.PathHafalan); os.IsNotExist(err) {
		os.MkdirAll(konst.PathHafalan, os.ModePerm)
	}

	f, err := os.OpenFile(konst.PathHafalan+haf.GetNamaHafalan(), os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return konst.ErrorReturn(w, "File Tidak Ditemukan", http.StatusBadGateway)
	}
	defer f.Close()

	_, err = io.Copy(f, file)
	if err != nil {
		return konst.ErrorReturn(w, "Gagal Upload File", http.StatusBadGateway)
	}

	c = ses.DB(konst.DBName).C(konst.DBHafalan)
	err = c.Insert(haf)
	if err != nil {
		return konst.ErrorReturn(w, "Tambah Database Gagal", http.StatusBadGateway)
	}

	return konst.SuccessReturn(w, "Upload Hafalan Berhasil", http.StatusOK)
}

func InsertCorrectionToHafalan(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session, idhafalan string, koreksi correction.Correction) (bool, string) {
	var haf hafalan.Hafalan
	var jwtt jwt.JWTMusyrif
	var use user.User

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return false, msg
	}

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBHafalan)

	err := c.Find(bson.M{"_id": bson.ObjectIdHex(idhafalan)}).One(&haf)
	if err != nil {
		return false, "Data Hafalan Tidak Ditemukan"
	}

	token := r.Header.Get(konst.HeaderToken)
	jwtt.SetToken(token)
	jwtt.GetMessageFromToken()
	iduser := jwtt.GetMessage()

	d := ses.DB(konst.DBName).C(konst.DBUser)
	err = d.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&use)
	if err != nil {
		return false, "Data User Tidak Ditemukan"
	}

	if use.GetLoginTypeInt() != 2 {
		return false, "Anda Tidak Boleh Memberikan Koreksi"
	}

	if use.GetUsername() != haf.GetUsernameUstaz() {
		return false, "Anda Tidak Berhak Memberikan Koreksi"
	}

	haf.SetKoreksi(koreksi)
	err = c.Update(bson.M{"_id": bson.ObjectIdHex(idhafalan)}, haf)
	if err != nil {
		return false, "Gagal Menambahkan Koreksi"
	}
	return true, "Berhasil Menambahkan Koreksi"
}

func DownloadHafalan(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session, idhafalan string) string {
	var haf hafalan.Hafalan
	var jwtt jwt.JWTMusyrif
	var use user.User

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBHafalan)

	err := c.Find(bson.M{"_id": bson.ObjectIdHex(idhafalan)}).One(&haf)
	if err != nil {
		return konst.ErrorReturn(w, "Data Hafalan Tidak Ditemukan", http.StatusBadRequest)
	}

	token := r.Header.Get(konst.HeaderToken)
	jwtt.SetToken(token)
	jwtt.GetMessageFromToken()
	iduser := jwtt.GetMessage()

	d := ses.DB(konst.DBName).C(konst.DBUser)
	err = d.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&use)
	if err != nil {
		return konst.ErrorReturn(w, "Data User Tidak Ditemukan", http.StatusBadRequest)
	}

	if use.GetLoginTypeInt() == 1 {
		d = ses.DB(konst.DBName).C(konst.DBSantri)
		var san santri.Santri
		err = d.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&san)
		if err != nil {
			return konst.ErrorReturn(w, "Data Santri Tidak Ditemukan", http.StatusBadRequest)
		}

		if haf.GetUsernameSantri() != san.GetUsername() {
			return konst.ErrorReturn(w, "Anda Tidak Boleh Mengunduh Rekaman Ini", http.StatusForbidden)
		}
	} else if use.GetLoginTypeInt() == 2 {
		d = ses.DB(konst.DBName).C(konst.DBUstaz)
		var ust ustaz.Ustaz
		err = d.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&ust)
		if err != nil {
			return konst.ErrorReturn(w, "Data Ustaz Tidak Ditemukan", http.StatusBadRequest)
		}

		if haf.GetUsernameUstaz() != ust.GetUsername() {
			return konst.ErrorReturn(w, "Anda Tidak Boleh Mengunduh Rekaman Ini", http.StatusForbidden)
		}
	}

	path := konst.PathHafalan + "/" + haf.GetNamaHafalan()

	if _, err := os.Stat(path); os.IsNotExist(err) {
		return konst.ErrorReturn(w, "File Tidak Ditemukan", http.StatusBadRequest)
	}

	http.ServeFile(w, r, path)
	return ""
}

func GetListHafalan(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session) string {
	var haf []hafalan.Hafalan
	var jwtt jwt.JWTMusyrif
	var use user.User

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBHafalan)

	// err := c.Find(bson.M{"_id": bson.ObjectIdHex(idhafalan)}).One(&haf)
	// if err != nil {
	// 	return konst.ErrorReturn(w, "Data Hafalan Tidak Ditemukan", http.StatusBadRequest)
	// }

	token := r.Header.Get(konst.HeaderToken)
	jwtt.SetToken(token)
	jwtt.GetMessageFromToken()
	iduser := jwtt.GetMessage()

	d := ses.DB(konst.DBName).C(konst.DBUser)
	err := d.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&use)
	if err != nil {
		return konst.ErrorReturn(w, "Data User Tidak Ditemukan", http.StatusBadRequest)
	}

	if use.GetLoginTypeInt() == 1 {
		err := c.Find(bson.M{"usernamesantri": use.GetUsername()}).All(&haf)
		if err != nil {
			return konst.ErrorReturn(w, "Data Hafalan Tidak Ditemukan", http.StatusBadRequest)
		}
	} else if use.GetLoginTypeInt() == 2 {
		err := c.Find(bson.M{"usernameustaz": use.GetUsername()}).All(&haf)
		if err != nil {
			return konst.ErrorReturn(w, "Data Hafalan Tidak Ditemukan", http.StatusBadRequest)
		}
	}

	jsonReturn, erro := json.Marshal(haf)
	if erro != nil {
		return konst.ErrorReturn(w, "JSON Return Gagal", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	return string(jsonReturn)

}

func HafalanController(w http.ResponseWriter, r *http.Request, urle string, s *session.Session) string {
	urle = urle[1:]
	pathe := strings.Split(urle, "/")

	ses, err := mgo.Dial("localhost:27017")
	if err != nil {
		konst.ErrorReturn(w, "Database Down", http.StatusBadGateway)
	}
	defer ses.Close()
	ses.SetMode(mgo.Monotonic, true)

	if pathe[1] == "upload" {
		return UploadHafalan(ses, w, r, s)
	} else if pathe[1] == "get" {
		return GetListHafalan(ses, w, r, s)
	} else if pathe[1] == "download" && pathe[2] != "" {
		return DownloadHafalan(ses, w, r, s, pathe[2])
	}

	return konst.ErrorReturn(w, "Path Tidak Ditemukan", http.StatusNotFound)
}

// func StreamingHafalan(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss session.Session, idhafalan string) string {
// 	var haf hafalan.Hafalan
// 	var jwtt jwt.JWTMusyrif
// 	var use user.User

// 	stat, msg := auth.CheckAuthorization(r, ss)
// 	if !stat {
// 		return konst.ErrorReturn(w, msg, http.StatusForbidden)
// 	}

// 	ses := s.Copy()
// 	defer ses.Close()
// 	c := ses.DB(konst.DBName).C(konst.DBHafalan)

// 	err := c.Find(bson.M{"_id": bson.ObjectIdHex(idhafalan)}).One(&haf)
// 	if err != nil {
// 		return konst.ErrorReturn(w, "Data Hafalan Tidak Ditemukan", http.StatusBadRequest)
// 	}

// 	token := r.Header.Get(konst.HeaderToken)
// 	jwtt.SetToken(token)
// 	jwtt.GetMessageFromToken()
// 	iduser := jwtt.GetMessage()

// 	d := ses.DB(konst.DBName).C(konst.DBUser)
// 	err = d.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&use)
// 	if err != nil {
// 		return konst.ErrorReturn(w, "Data User Tidak Ditemukan", http.StatusBadRequest)
// 	}

// 	if use.GetLoginTypeInt() == 1 {
// 		d = ses.DB(konst.DBName).C(konst.DBSantri)
// 		var san santri.Santri
// 		err = d.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&san)
// 		if err != nil {
// 			return konst.ErrorReturn(w, "Data Santri Tidak Ditemukan", http.StatusBadRequest)
// 		}

// 		if haf.GetUsernameSantri() != san.GetUsername() {
// 			return konst.ErrorReturn(w, "Anda Tidak Boleh Mengunduh Rekaman Ini", http.StatusForbidden)
// 		}
// 	} else if use.GetLoginTypeInt() == 2 {
// 		d = ses.DB(konst.DBName).C(konst.DBUstaz)
// 		var ust ustaz.Ustaz
// 		err = d.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&ust)
// 		if err != nil {
// 			return konst.ErrorReturn(w, "Data Ustaz Tidak Ditemukan", http.StatusBadRequest)
// 		}

// 		if haf.GetUsernameUstaz() != ust.GetUsername() {
// 			return konst.ErrorReturn(w, "Anda Tidak Boleh Mengunduh Rekaman Ini", http.StatusForbidden)
// 		}
// 	}

// 	path := konst.PathHafalan + "/" + haf.GetNamaHafalan()

// 	if _, err := os.Stat(path); os.IsNotExist(err) {
// 		return konst.ErrorReturn(w, "File Tidak Ditemukan", http.StatusBadRequest)
// 	}

// 	http.ServeFile(w, r, path)
// 	return ""
// }
