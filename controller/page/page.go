package pagee

import (
	"encoding/json"
	"net/http"
	"os"
	"strings"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"../../models/page"
	"../../models/session"

	"../../auth"
	"../../konst"
)

func DownloadAll(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session) string {
	// var dataFile []map[string]string
	var file page.Page

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBAlquranImage)

	iter := c.Find(bson.M{}).Iter()
	// if err != nil {
	// 	return konst.ErrorReturn(w, "Data Tidak Ditemukan", http.StatusBadRequest)
	// }
	for iter.Next(&file) {
		path := konst.PathAlquran + file.GetNamaGambar()
		if _, err := os.Stat(path); os.IsNotExist(err) {
			return konst.ErrorReturn(w, "File Tidak Ditemukan", http.StatusBadRequest)
		}
		http.ServeFile(w, r, path)
	}
	return ""
	// return konst.SuccessReturn(w, "Semua File Telah Terdownload", http.StatusOK)
}

func DownloadPage(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session, imgstring string) string {
	var file page.Page

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBAlquranImage)

	err := c.Find(bson.M{"namagambar": imgstring}).One(&file)
	if err != nil {
		return konst.ErrorReturn(w, "Request Gagal", http.StatusBadRequest)
	}

	path := konst.PathAlquran + file.GetNamaGambar()
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return konst.ErrorReturn(w, "File Tidak Ditemukan", http.StatusBadRequest)
	}
	http.ServeFile(w, r, path)
	return ""
	// return konst.SuccessReturn(w, "File Terdownload", http.StatusOK)
}

//Format permintaan :{"surahawal":,"ayatawal":,"surahakhir":,"ayatakhir":}
func GetPage(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session) string {
	var pageawal, pageakhir []int
	var pages []int
	var reqPage map[string]interface{}
	retPage := make(map[string]interface{})

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	err := json.NewDecoder(r.Body).Decode(&reqPage)
	if err != nil {
		return konst.ErrorReturn(w, "Format Request Salah", http.StatusBadRequest)
	}

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBAlquranImage)

	surahawal := int(reqPage["surahawal"].(float64))
	ayatawal := int(reqPage["ayatawal"].(float64))
	surahakhir := int(reqPage["surahakhir"].(float64))
	ayatakhir := int(reqPage["ayatakhir"].(float64))

	// err = c.Find(bson.M{"$and": []bson.M{bson.M{"$and": []bson.M{bson.M{"namasurat": bson.M{"$gte": surahawal}}, bson.M{"ayatawal": bson.M{"$gte": ayatawal}}}}, bson.M{"$and": []bson.M{bson.M{"namasurat": bson.M{"$lte": surahakhir}}, bson.M{"ayatawal": bson.M{"$lte": ayatakhir}}}}}}).Distinct("halaman", &pages)
	// err = c.Find(bson.M{"$or": []bson.M{bson.M{"$and": []bson.M{bson.M{"namasurat": surahawal}, bson.M{"$and": []bson.M{bson.M{"ayatawal": bson.M{"$lte": ayatawal}}, bson.M{"ayatakhir": bson.M{"$gte": ayatawal}}}}}}, bson.M{"$and": []bson.M{bson.M{"namasurat": surahakhir}, bson.M{"$and": []bson.M{bson.M{"ayatawal": bson.M{"$lte": ayatakhir}}, bson.M{"ayatakhir": bson.M{"$gte": ayatakhir}}}}}}}}).Distinct("halaman", &pages)
	err = c.Find(bson.M{"$and": []bson.M{bson.M{"namasurat": surahawal}, bson.M{"$and": []bson.M{bson.M{"ayatawal": bson.M{"$lte": ayatawal}}, bson.M{"ayatakhir": bson.M{"$gte": ayatawal}}}}}}).Distinct("halaman", &pageawal)
	if err != nil {
		// fmt.Println("Error PageAwal")
		return konst.ErrorReturn(w, "Data Halaman Tidak Ditemukan", http.StatusBadRequest)
	}
	err = c.Find(bson.M{"$and": []bson.M{bson.M{"namasurat": surahakhir}, bson.M{"$and": []bson.M{bson.M{"ayatawal": bson.M{"$lte": ayatakhir}}, bson.M{"ayatakhir": bson.M{"$gte": ayatakhir}}}}}}).Distinct("halaman", &pageakhir)
	if err != nil {
		// fmt.Println("Error PageAkhir")
		return konst.ErrorReturn(w, "Data Halaman Tidak Ditemukan", http.StatusBadRequest)
	}

	if len(pageawal) == 0 || len(pageakhir) == 0 {
		return konst.ErrorReturn(w, "Data Halaman Tidak Ditemukan", http.StatusBadRequest)
	}

	// fmt.Println(pageawal)
	// fmt.Println(pageakhir)

	// sort.Ints(pages)

	for i := pageawal[0]; i <= pageakhir[0]; i++ {
		pages = append(pages, i)
	}

	retPage["halaman"] = pages

	jsonPage, erro := json.Marshal(retPage)
	if erro != nil {
		return konst.ErrorReturn(w, "Gagal JSON Page", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	return string(jsonPage)
}

func PageController(w http.ResponseWriter, r *http.Request, urle string, s *session.Session) string {
	urle = urle[1:]
	pathe := strings.Split(urle, "/")

	ses, err := mgo.Dial("localhost:27017")
	if err != nil {
		konst.ErrorReturn(w, "Database Down", http.StatusBadGateway)
	}
	defer ses.Close()
	ses.SetMode(mgo.Monotonic, true)

	if pathe[1] == "get" {
		return GetPage(ses, w, r, s)
	} else if pathe[1] == "download" {
		if pathe[2] == "all" {
			return DownloadAll(ses, w, r, s)
		} else {
			return DownloadPage(ses, w, r, s, pathe[3])
		}
	}

	return konst.ErrorReturn(w, "Path Tidak Ditemukan", http.StatusNotFound)
}
