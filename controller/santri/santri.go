package santris

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"os"

	"../../konst"
	"../../models/santri"
	"../../models/ustaz"

	"mime/multipart"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//Format dataRegis: {"username":,"password":,"password_again":,"nama":,"noid":,"jenisid":,"jk":,"email":,"nohp":,"role":}
func CheckDupSantri(s *mgo.Session, dataRegis map[string]interface{}) (bool, string) {
	msg := ""
	ret := true

	ses := s.Copy()
	defer ses.Close()

	c := ses.DB(konst.DBName).C(konst.DBSantri)

	sum, _ := c.Find(bson.M{"noid": dataRegis["noid"].(string)}).Count()
	if sum > 0 {
		ret = false
		msg = msg + "No ID"
	}

	sum, _ = c.Find(bson.M{"email": dataRegis["email"].(string)}).Count()
	if sum > 0 {
		ret = false
		if !ret {
			msg = msg + ", Email"
		} else {
			msg = msg + "Email"
		}
	}

	sum, _ = c.Find(bson.M{"nohp": dataRegis["nohp"].(string)}).Count()
	if sum > 0 {
		ret = false
		if !ret {
			msg = msg + ", No Handphone"
		} else {
			msg = msg + "No Handphone"
		}
	}

	if !ret {
		msg = msg + " Telah Digunakan."
	}

	return ret, msg
}

func SantriDelete(s *mgo.Session, dataDelete map[string]interface{}, iduser string) (bool, string) {
	var santri santri.Santri
	var ust ustaz.Ustaz

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBSantri)

	err := c.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&santri)
	if err != nil {
		return false, "Data Santri Tidak Ditemukan"
	}

	if dataDelete["password"].(string) != dataDelete["password_again"].(string) || dataDelete["password"].(string) != santri.GetPassword() {
		return false, "Password Salah"
	}

	if santri.GetUsernameUstaz() != "" {
		d := ses.DB(konst.DBName).C(konst.DBUstaz)
		_ = d.Find(bson.M{"username": santri.GetUsernameUstaz()}).One(&ust)
		stat := ust.DeleteUsernameSantri(ses, santri.GetUsername())
		if !stat {
			return false, "Gagal Hapus Data di Ustaz"
		}
	}

	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(iduser)})
	if err != nil {
		return false, "Gagal Hapus Data"
	}

	return true, "Berhasil Hapus Data"
}

func SantriEdit(s *mgo.Session, dataEdit map[string]interface{}, iduser string) (bool, string) {
	var santri santri.Santri
	var bsonn map[string]interface{}

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBSantri)

	jsonEdit, _ := json.Marshal(dataEdit)
	err := json.Unmarshal(jsonEdit, &santri)
	if err != nil {
		return false, "Tidak Ada Request Edit"
	}

	if santri.GetId() != "" {
		santri.SetId("")
	}

	jsonSantri, _ := json.Marshal(santri)
	err = json.Unmarshal(jsonSantri, &bsonn)
	if err != nil {
		return false, "Tidak Ada Request Edit"
	}

	err = c.Update(bson.M{"_id": bson.ObjectIdHex(iduser)}, bson.M{"$set": bsonn})
	if err != nil {
		return false, "Gagal Edit Data"
	}

	return true, "Berhasil Edit Data"
}

func SantriRegistration(s *mgo.Session, dataRegis map[string]interface{}, fotoprofil multipart.File, headerfoto *multipart.FileHeader) (bool, string) {
	var santrii santri.Santri
	statfoto := false

	ses := s.Copy()
	defer ses.Close()

	stat, msg := CheckDupSantri(ses, dataRegis)
	if !stat {
		return stat, msg
	}

	// fmt.Println(fotoprofil)
	// fmt.Println(headerfoto)

	if fotoprofil != nil || headerfoto != nil {
		statfoto = true
	}

	if statfoto {
		dataRegis["fotoprofil"] = dataRegis["username"].(string) + headerfoto.Filename[len(headerfoto.Filename)-4:]
	}

	encryptPass := sha256.Sum256([]byte(dataRegis["password"].(string)))
	dataRegis["password"] = fmt.Sprintf("%x", encryptPass)
	santrii.CreateSantriReg(dataRegis)

	if statfoto {
		if _, err := os.Stat(konst.PathPPSantri); os.IsNotExist(err) {
			// fmt.Println(konst.PathPPSantri + " Tidak Exist")
			os.MkdirAll(konst.PathPPSantri, os.ModePerm)
		}

		// fmt.Println(konst.PathPPSantri + santrii.GetFotoProfil())

		f, err := os.OpenFile(konst.PathPPSantri+santrii.GetFotoProfil(), os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			return false, "File Foto Santri Tidak Ditemukan"
		}
		defer f.Close()

		_, err = io.Copy(f, fotoprofil)
		if err != nil {
			return false, "Gagal Upload File Foto Santri"
		}
	}

	c := ses.DB(konst.DBName).C(konst.DBSantri)
	fmt.Println(santrii)
	err := c.Insert(santrii)
	if err != nil {
		return false, "Database Error"
	}

	err = c.Find(bson.M{"username": santrii.GetUsername()}).One(&santrii)
	if err != nil {
		fmt.Println("Santri Error")
		return false, "Database Error"
	}
	fmt.Println("Berhasil Santri")
	return true, santrii.GetId()
}

//Membuka profil dari santri
//Status menjelaskan apakah yang membuka adalah pemilik akun atau bukan.. 1 jika iya 0 jika bukan
func GetProfile(s *mgo.Session, username string, status int) (bool, string) {
	var santri santri.Santri
	var ust ustaz.Ustaz
	// var jumust int

	ses := s.Copy()
	defer ses.Close()

	c := ses.DB(konst.DBName).C(konst.DBSantri)

	if status == 0 {
		err := c.Find(bson.M{"username": username}).One(&santri)
		if err != nil {
			return false, "Data Santri Tidak Ditemukan"
		}
		returnSantri := santri.SantriNotSelf()
		jsonSantri, _ := json.Marshal(returnSantri)
		return true, string(jsonSantri)
	}

	err := c.Find(bson.M{"username": username}).Select(bson.M{"_id": 0, "password": 0}).One(&santri)
	if err != nil {
		return false, "Data Santri Tidak Ditemukan"
	}
	returnSantri := make(map[string]interface{})
	returnSantri["username"] = santri.GetUsername()
	returnSantri["nama"] = santri.GetNama()
	returnSantri["noid"] = santri.GetNoId()
	returnSantri["jenisid"] = santri.GetJenisId()
	returnSantri["jk"] = santri.GetJK()
	returnSantri["email"] = santri.GetEmail()
	returnSantri["nohp"] = santri.GetNoHP()
	returnSantri["fotoprofil"] = santri.GetFotoProfil()

	d := ses.DB(konst.DBName).C(konst.DBUstaz)
	if santri.GetUsernameUstaz() != "" {
		err = d.Find(bson.M{"username": santri.GetUsernameUstaz()}).One(&ust)
		returnUstaz := make(map[string]interface{})
		returnUstaz["username"] = ust.GetUsername()
		returnUstaz["nama"] = ust.GetNama()
		returnUstaz["jk"] = ust.GetJK()
		returnUstaz["email"] = ust.GetEmail()
		returnUstaz["nohp"] = ust.GetNoHP()
		returnUstaz["fotoprofil"] = ust.GetFotoProfil()
		returnSantri["ustaz"] = returnUstaz
	}

	jsonSantri, _ := json.Marshal(returnSantri)
	return true, string(jsonSantri)
}
