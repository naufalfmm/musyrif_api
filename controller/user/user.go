package users

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"../../konst"
	"../../models/jwt"
	"../../models/santri"
	"../../models/session"
	"../../models/user"
	"../../models/ustaz"

	"../santri"
	"../ustaz"

	"mime/multipart"

	"../../auth"
)

func CheckDup(s *mgo.Session, dataRegis map[string]interface{}) (bool, string) {
	ses := s.Copy()
	defer ses.Close()

	c := ses.DB(konst.DBName).C(konst.DBUser)

	sum, _ := c.Find(bson.M{"username": dataRegis["username"].(string)}).Count()
	if sum > 0 {
		return false, "Username Telah Digunakan"
	}

	return true, ""
}

//Format json {"password":, "password_again":}
func DeleteUser(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session) string {
	var user user.User
	var jwtt jwt.JWTMusyrif
	var dataDelete map[string]interface{}

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	err := json.NewDecoder(r.Body).Decode(&dataDelete)
	if err != nil {
		return konst.ErrorReturn(w, "Format Request Salah", http.StatusBadRequest)
	}

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBUser)

	token := r.Header.Get(konst.HeaderToken)
	jwtt.SetToken(token)
	jwtt.GetMessageFromToken()
	iduser := jwtt.GetMessage()

	err = c.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&user)
	if err != nil {
		return konst.ErrorReturn(w, "Data User Tidak Ditemukan", http.StatusBadRequest)
	}

	dataDelete["password"] = fmt.Sprintf("%x", sha256.Sum256([]byte(dataDelete["password"].(string))))
	dataDelete["password_again"] = fmt.Sprintf("%x", sha256.Sum256([]byte(dataDelete["password_again"].(string))))

	if dataDelete["password"].(string) != dataDelete["password_again"].(string) || dataDelete["password"].(string) != user.GetPassword() {
		return konst.ErrorReturn(w, "Password Salah", http.StatusForbidden)
	}

	if user.GetLoginTypeInt() == 2 {
		che, ret := ustazs.UstazDelete(ses, dataDelete, iduser)
		if !che {
			return konst.ErrorReturn(w, ret, http.StatusBadRequest)
		}
	} else if user.GetLoginTypeInt() == 1 {
		che, ret := santris.SantriDelete(ses, dataDelete, iduser)
		if !che {
			return konst.ErrorReturn(w, ret, http.StatusBadRequest)
		}
	}

	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(iduser)})
	if err != nil {
		return konst.ErrorReturn(w, "Gagal Hapus Data", http.StatusInternalServerError)
	}

	sesi := r.Header.Get(konst.HeaderSession)
	auth.LogoutSession(ss, sesi)
	return konst.SuccessReturn(w, "Data Berhasil Dihapus", http.StatusOK)
}

func EditUser(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session) string {
	var dataEdit map[string]interface{}
	var user user.User
	var jwtt jwt.JWTMusyrif
	var acc bool
	var pes string

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	err := json.NewDecoder(r.Body).Decode(&dataEdit)
	if err != nil {
		return konst.ErrorReturn(w, "Format Request Salah", http.StatusBadRequest)
	}

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBUser)

	token := r.Header.Get(konst.HeaderToken)
	jwtt.SetToken(token)
	jwtt.GetMessageFromToken()
	iduser := jwtt.GetMessage()

	err = c.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&user)
	if err != nil {
		return konst.ErrorReturn(w, "Anda Belum Registrasi", http.StatusBadRequest)
	}

	if user.GetLoginTypeInt() == 2 {
		acc, pes = ustazs.UstazEdit(ses, dataEdit, iduser)
		if !acc {
			return konst.ErrorReturn(w, pes, http.StatusBadRequest)
		}
	} else if user.GetLoginTypeInt() == 1 {
		acc, pes = santris.SantriEdit(ses, dataEdit, iduser)
		if !acc {
			return konst.ErrorReturn(w, pes, http.StatusBadRequest)
		}
	}
	return konst.SuccessReturn(w, pes, http.StatusOK)
}

//Format json pengiriman {"username":,"password":,"password_again":,"nama":,"noid":,"jenisid":,"jk":,"email":,"nohp":,"role":}
func UserRegistration(s *mgo.Session, w http.ResponseWriter, r *http.Request) string {
	var user user.User
	dataRegis := make(map[string]interface{})
	var file multipart.File
	var header *multipart.FileHeader

	ses := s.Copy()
	defer ses.Close()

	c := ses.DB(konst.DBName).C(konst.DBUser)

	file, header, err := r.FormFile("fotoprofil")
	fmt.Println(file)
	fmt.Println(header)
	if err != nil {
		file = nil
		header = nil
	} else {
		defer file.Close()
	}

	jsonn := r.FormValue("jsonregistrasi")

	fmt.Println(jsonn)

	// err := json.NewDecoder(r.Body).Decode(&dataRegis)
	// jsonnn,_ := json.Marshal(jsonn)
	// fmt.Println(jsonnn[1:len(jsonnn)-1])
	err = json.Unmarshal([]byte(jsonn), &dataRegis)
	fmt.Println([]byte(jsonn))
	if err != nil {
		return konst.ErrorReturn(w, "Registrasi Gagal", http.StatusBadRequest)
	}

	if dataRegis["password"].(string) != dataRegis["password_again"].(string) {
		return konst.ErrorReturn(w, "Password Tidak Sama", http.StatusBadRequest)
	}

	if emailvalid := auth.CheckEmail(dataRegis["email"].(string)); !emailvalid {
		return konst.ErrorReturn(w, "Format Email Salah", http.StatusBadRequest)
	}

	stat, msg := CheckDup(ses, dataRegis)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusBadRequest)
	}

	if int(dataRegis["role"].(float64)) == 2 {
		succ, id := ustazs.UstazRegistration(ses, dataRegis, file, header)
		if !succ {
			return konst.ErrorReturn(w, id, http.StatusInternalServerError)
		}
		dataRegis["id"] = id
		user.CreateUserReg(dataRegis)
		err := c.Insert(user)
		if err != nil {
			return konst.ErrorReturn(w, "Database Error", http.StatusInternalServerError)
		}
	} else if int(dataRegis["role"].(float64)) == 1 {
		succ, id := santris.SantriRegistration(ses, dataRegis, file, header)
		if !succ {
			return konst.ErrorReturn(w, id, http.StatusInternalServerError)
		}
		dataRegis["id"] = id
		user.CreateUserReg(dataRegis)
		err := c.Insert(user)
		if err != nil {
			return konst.ErrorReturn(w, "Database Error", http.StatusInternalServerError)
		}
	}
	return konst.SuccessReturn(w, "Registrasi Berhasil", http.StatusOK)
}

func GetUserProfile(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session, usernamepath string) string {
	var user user.User
	var jwtt jwt.JWTMusyrif
	var returnUser string

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBUser)

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}
	// if !stat {
	// 	err := c.Find(bson.M{"username": usernamepath}).One(&user)
	// 	if err != nil {
	// 		return konst.ErrorReturn(w, "Data User Tidak Ditemukan", http.StatusBadRequest)
	// 	}
	// 	if user.GetLoginTypeInt() == 2 {
	// 		stat, msg := ustazs.GetProfile(ses, usernamepath, 0)
	// 		if !stat {
	// 			return konst.ErrorReturn(w, msg, http.StatusBadRequest)
	// 		}
	// 		returnUser = msg
	// 	} else if user.GetLoginTypeInt() == 1 {
	// 		stat, msg := santris.GetProfile(ses, usernamepath, 0)
	// 		if !stat {
	// 			return konst.ErrorReturn(w, msg, http.StatusBadRequest)
	// 		}
	// 		returnUser = msg
	// 	}
	// } else {
	token := r.Header.Get(konst.HeaderToken)
	jwtt.SetToken(token)
	jwtt.GetMessageFromToken()
	iduser := jwtt.GetMessage()

	err := c.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&user)
	if err != nil {
		return konst.ErrorReturn(w, "Data User Tidak Ditemukan", http.StatusBadRequest)
	}

	if user.GetUsername() == usernamepath {
		if user.GetLoginTypeInt() == 2 {
			stat, msg := ustazs.GetProfile(ses, usernamepath, 1)
			if !stat {
				return konst.ErrorReturn(w, msg, http.StatusBadRequest)
			}
			returnUser = msg
		} else if user.GetLoginTypeInt() == 1 {
			stat, msg := santris.GetProfile(ses, usernamepath, 1)
			if !stat {
				return konst.ErrorReturn(w, msg, http.StatusBadRequest)
			}
			returnUser = msg
		}
	} else {
		if user.GetLoginTypeInt() == 2 {
			stat, msg := ustazs.GetProfile(ses, usernamepath, 0)
			if !stat {
				return konst.ErrorReturn(w, msg, http.StatusBadRequest)
			}
			returnUser = msg
		} else if user.GetLoginTypeInt() == 1 {
			stat, msg := santris.GetProfile(ses, usernamepath, 0)
			if !stat {
				return konst.ErrorReturn(w, msg, http.StatusBadRequest)
			}
			returnUser = msg
		}
	}
	// }
	w.WriteHeader(http.StatusOK)
	return returnUser
}

func UserLogout(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session) string {

	ses := s.Copy()
	defer ses.Close()

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	sessid := r.Header.Get(konst.HeaderSession)
	ss.DeleteSession(sessid)
	return konst.SuccessReturn(w, "Anda Berhasil Logout", http.StatusOK)
}

func UserLogin(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session) string {
	var user user.User

	ses := s.Copy()
	defer ses.Close()

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		return konst.ErrorReturn(w, "Login Gagal", http.StatusBadRequest)
	}

	c := ses.DB(konst.DBName).C(konst.DBUser)

	encryptPassLogin := fmt.Sprintf("%x", sha256.Sum256([]byte(user.GetPassword())))

	err = c.Find(bson.M{"username": user.GetUsername()}).One(&user)
	if err != nil {
		return konst.ErrorReturn(w, "Anda Belum Registrasi", http.StatusBadRequest)
	}

	encryptPass := user.GetPassword()

	if encryptPass != encryptPassLogin {
		return konst.ErrorReturn(w, "Password Salah", http.StatusForbidden)
	}

	stat, role := user.GetLoginType()
	if !stat {
		return konst.ErrorReturn(w, role, http.StatusBadRequest)
	}

	stat, msg := auth.LoginSession(ss, user)

	if !stat {
		// w.WriteHeader(http.StatusAccepted)
		// return fmt.Sprintf("{\"session\":\"%s\"}", msg)
		return konst.ErrorReturn(w, "Anda Sudah Login", http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusOK)
	return fmt.Sprintf("{\"token\": \"%s\", \"username\": \"%s\", \"role\": \"%s\", \"session\": \"%s\"}", auth.LoginToken(user), user.GetUsername(), role, msg)
}

func GetMySession(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session) string {
	var user user.User

	ses := s.Copy()
	defer ses.Close()

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		return konst.ErrorReturn(w, "Format Request Salah", http.StatusBadRequest)
	}

	c := ses.DB(konst.DBName).C(konst.DBUser)

	encryptPassLogin := fmt.Sprintf("%x", sha256.Sum256([]byte(user.GetPassword())))

	err = c.Find(bson.M{"username": user.GetUsername()}).One(&user)
	if err != nil {
		return konst.ErrorReturn(w, "Anda Belum Registrasi", http.StatusBadRequest)
	}

	encryptPass := user.GetPassword()

	if encryptPass != encryptPassLogin {
		return konst.ErrorReturn(w, "Password Salah", http.StatusForbidden)
	}

	_, msg := auth.LoginSession(ss, user)

	w.WriteHeader(http.StatusAccepted)
	return fmt.Sprintf("{\"session\": \"%s\"}", msg)
}

func GetProfilePhoto(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session, imgfile string) string {
	var us user.User
	var usta map[string]interface{}
	var san map[string]interface{}
	var sant santri.Santri
	var ustt ustaz.Ustaz
	var jwtt jwt.JWTMusyrif

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	token := r.Header.Get(konst.HeaderToken)
	jwtt.SetToken(token)
	jwtt.GetMessageFromToken()
	iduser := jwtt.GetMessage()

	ses := s.Copy()
	defer ses.Close()

	c := ses.DB(konst.DBName).C(konst.DBUser)

	err := c.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&us)
	if err != nil {
		return konst.ErrorReturn(w, "Data User Tidak Ditemukan", http.StatusBadRequest)
	}

	if us.GetLoginTypeInt() == 2 {
		stat, ust := ustazs.GetProfile(ses, us.GetUsername(), 1)
		if !stat {
			return konst.ErrorReturn(w, ust, http.StatusBadRequest)
		}
		_ = json.Unmarshal([]byte(ust), &usta)
		if usta["fotoprofil"].(string) == imgfile {
			http.ServeFile(w, r, konst.PathPPUstaz+imgfile)
			return ""
		} else {
			listsantri := usta["usernamesantri"].([]string)
			d := ses.DB(konst.DBName).C(konst.DBSantri)
			for i := 0; i < len(listsantri); i++ {
				erro := d.Find(bson.M{"username": listsantri[i]}).One(&sant)
				if erro != nil {
					return konst.ErrorReturn(w, "Data Santri Tidak Ditemukan", http.StatusBadRequest)
				}
				if sant.GetFotoProfil() == imgfile {
					http.ServeFile(w, r, konst.PathPPSantri+imgfile)
					return ""
				}
			}
		}
	} else if us.GetLoginTypeInt() == 1 {
		stat, sann := santris.GetProfile(ses, us.GetUsername(), 1)
		if !stat {
			return konst.ErrorReturn(w, sann, http.StatusBadRequest)
		}
		fmt.Println(sann)
		_ = json.Unmarshal([]byte(sann), &san)
		fmt.Println(san)
		if san["fotoprofil"].(string) == imgfile {
			http.ServeFile(w, r, konst.PathPPSantri+imgfile)
			return ""
		} else {
			usernameustaz := san["usernameustaz"]
			d := ses.DB(konst.DBName).C(konst.DBUstaz)
			erro := d.Find(bson.M{"username": usernameustaz}).One(&ustt)
			if erro != nil {
				return konst.ErrorReturn(w, "Data Ustaz Tidak Ditemukan", http.StatusBadRequest)
			}
			if ustt.GetFotoProfil() == imgfile {
				http.ServeFile(w, r, konst.PathPPUstaz+imgfile)
				return ""
			}
		}
	}
	return konst.ErrorReturn(w, "Anda Tidak Berhak", http.StatusForbidden)

}

type ust map[string]interface{}

func GetUstazList(s *mgo.Session, w http.ResponseWriter, r *http.Request, ss *session.Session) string {
	var san santri.Santri
	var jwtt jwt.JWTMusyrif
	var ustt []ust
	var returnUstazz []ust
	returnUstaz := make(map[string]interface{})

	stat, msg := auth.CheckAuthorization(r, ss)
	if !stat {
		return konst.ErrorReturn(w, msg, http.StatusForbidden)
	}

	token := r.Header.Get(konst.HeaderToken)
	jwtt.SetToken(token)
	jwtt.GetMessageFromToken()
	iduser := jwtt.GetMessage()

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBSantri)
	err := c.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&san)
	if err != nil {
		return konst.ErrorReturn(w, "Data Santri Tidak Ditemukan", http.StatusForbidden)
	}

	if san.GetUsernameUstaz() != "" {
		return konst.ErrorReturn(w, "Anda Sudah Memilih Ustaz", http.StatusBadRequest)
	}

	d := ses.DB(konst.DBName).C(konst.DBUstaz)
	err = d.Find(nil).All(&ustt)
	if err != nil {
		return konst.ErrorReturn(w, "Data Ustaz Tidak Ada", http.StatusBadRequest)
	}

	for i := 0; i < len(ustt); i++ {
		usttt := make(map[string]interface{})
		usttt["username"] = ustt[i]["username"]
		usttt["nama"] = ustt[i]["nama"]
		usttt["jk"] = ustt[i]["jk"]
		usttt["email"] = ustt[i]["email"]
		usttt["nohp"] = ustt[i]["nohp"]
		usttt["fotoprofil"] = ustt[i]["fotoprofil"]
		returnUstazz = append(returnUstazz, usttt)
	}
	returnUstaz["ustaz"] = returnUstazz
	jsonUstaz, _ := json.Marshal(returnUstaz)
	w.WriteHeader(http.StatusOK)
	return string(jsonUstaz)
}

func UserController(w http.ResponseWriter, r *http.Request, urle string, s *session.Session) string {
	urle = urle[1:]
	pathe := strings.Split(urle, "/")

	ses, err := mgo.Dial("localhost:27017")
	if err != nil {
		return konst.ErrorReturn(w, "Database Down", http.StatusBadGateway)
	}
	defer ses.Close()
	ses.SetMode(mgo.Monotonic, true)

	if pathe[0] == "registrasi" {
		return UserRegistration(ses, w, r)
	} else if pathe[0] == "login" {
		return UserLogin(ses, w, r, s)
	} else if pathe[0] == "logout" {
		return UserLogout(ses, w, r, s)
	} else if pathe[0] == "user" && len(pathe) >= 2 {
		if pathe[1] == "edit" {
			return EditUser(ses, w, r, s)
		} else if pathe[1] == "session" {
			return GetMySession(ses, w, r, s)
		} else if pathe[1] == "delete" {
			return DeleteUser(ses, w, r, s)
		} else if pathe[1] == "photo" && pathe[2] != "" {
			return GetProfilePhoto(ses, w, r, s, pathe[2])
		} else if pathe[1] != "" {
			return GetUserProfile(ses, w, r, s, pathe[1])
		}
	} else if pathe[0] == "ustaz" && pathe[1] == "all" {
		return GetUstazList(ses, w, r, s)
	}

	return konst.ErrorReturn(w, "Path Tidak Ditemukan", http.StatusNotFound)
}
