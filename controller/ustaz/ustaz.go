package ustazs

import (
	"io"
	"os"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"encoding/json"

	"../../models/santri"
	"../../models/ustaz"

	"crypto/sha256"
	"fmt"

	"mime/multipart"

	"../../konst"
)

//Format dataRegis: {"username":,"password":,"password_again":,"nama":,"noid":,"jenisid":,"jk":,"email":,"nohp":,"role":}
func CheckDupUstaz(s *mgo.Session, dataRegis map[string]interface{}) (bool, string) {
	msg := ""
	ret := true

	ses := s.Copy()
	defer ses.Close()

	c := ses.DB(konst.DBName).C(konst.DBUstaz)

	sum, _ := c.Find(bson.M{"noid": dataRegis["noid"].(string)}).Count()
	if sum > 0 {
		ret = false
		msg = msg + "No ID"
	}

	sum, _ = c.Find(bson.M{"email": dataRegis["email"].(string)}).Count()
	if sum > 0 {
		ret = false
		if !ret {
			msg = msg + ", Email"
		} else {
			msg = msg + "Email"
		}
	}

	sum, _ = c.Find(bson.M{"nohp": dataRegis["nohp"].(string)}).Count()
	if sum > 0 {
		ret = false
		if !ret {
			msg = msg + ", No Handphone"
		} else {
			msg = msg + "No Handphone"
		}
	}

	if !ret {
		msg = msg + " Telah Digunakan."
	}

	return ret, msg
}

func UstazDelete(s *mgo.Session, dataDelete map[string]interface{}, iduser string) (bool, string) {
	var ustaz ustaz.Ustaz
	var san santri.Santri

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBUstaz)

	err := c.Find(bson.M{"_id": bson.ObjectIdHex(iduser)}).One(&ustaz)
	if err != nil {
		return false, "Data Ustaz Tidak Ditemukan"
	}

	if dataDelete["password"].(string) != dataDelete["password_again"].(string) || dataDelete["password"].(string) != ustaz.GetPassword() {
		return false, "Password Salah"
	}

	d := ses.DB(konst.DBName).C(konst.DBSantri)
	dataSantri := ustaz.GetUsernameSantri()
	for i := 0; i < len(dataSantri); i++ {
		_ = d.Find(bson.M{"username": dataSantri[i]}).One(&san)
		stat := san.DeleteUsernameUstaz(ses)
		if !stat {
			return false, "Gagal Hapus Data di Santri"
		}
	}

	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(iduser)})
	if err != nil {
		return false, "Gagal Hapus Data"
	}

	return true, "Berhasil Hapus Data"
}

func UstazEdit(s *mgo.Session, dataEdit map[string]interface{}, iduser string) (bool, string) {
	var ustaz ustaz.Ustaz
	var bsonn map[string]interface{}

	ses := s.Copy()
	defer ses.Close()
	c := ses.DB(konst.DBName).C(konst.DBUstaz)

	jsonEdit, _ := json.Marshal(dataEdit)
	err := json.Unmarshal(jsonEdit, &ustaz)
	if err != nil {
		return false, "Tidak Ada Request Edit"
	}

	if ustaz.GetId() != "" {
		ustaz.SetId("")
	}

	jsonUstaz, _ := json.Marshal(ustaz)
	err = json.Unmarshal(jsonUstaz, &bsonn)
	if err != nil {
		return false, "Tidak Ada Request Edit"
	}

	err = c.Update(bson.M{"_id": bson.ObjectIdHex(iduser)}, bson.M{"$set": bsonn})
	if err != nil {
		return false, "Gagal Edit Data"
	}

	return true, "Berhasil Edit Data"
}

func UstazRegistration(s *mgo.Session, dataRegis map[string]interface{}, fotoprofil multipart.File, headerfoto *multipart.FileHeader) (bool, string) {
	var ustaz ustaz.Ustaz

	ses := s.Copy()
	defer ses.Close()

	stat, msg := CheckDupUstaz(ses, dataRegis)
	if !stat {
		return stat, msg
	}

	if fotoprofil == nil || headerfoto == nil {
		return false, "Foto Profil Tidak Ada"
	}

	dataRegis["fotoprofil"] = dataRegis["username"].(string) + headerfoto.Filename[len(headerfoto.Filename)-4:]

	encryptPass := sha256.Sum256([]byte(dataRegis["password"].(string)))
	dataRegis["password"] = fmt.Sprintf("%x", encryptPass)
	ustaz.CreateUstazReg(dataRegis)

	if _, err := os.Stat(konst.PathPPUstaz); os.IsNotExist(err) {
		os.Mkdir(konst.PathHafalan, os.ModeDir)
	}

	f, err := os.OpenFile(konst.PathPPUstaz+ustaz.GetFotoProfil(), os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return false, "File Foto Ustaz Tidak Ditemukan"
	}
	defer f.Close()

	_, err = io.Copy(f, fotoprofil)
	if err != nil {
		return false, "Gagal Upload File Foto Ustaz"
	}

	c := ses.DB(konst.DBName).C(konst.DBUstaz)

	err = c.Insert(ustaz)
	if err != nil {
		return false, "Database Error"
	}

	err = c.Find(bson.M{"nama": ustaz.GetNama()}).One(&ustaz)
	if err != nil {
		return false, "Database Error"
	}

	return true, ustaz.GetId()
}

type santrii map[string]interface{}

//Membuka profil dari ustaz
//Status menjelaskan apakah yang membuka adalah pemilik akun atau bukan.. 1 jika iya 0 jika bukan
func GetProfile(s *mgo.Session, username string, status int) (bool, string) {
	var ustaz ustaz.Ustaz
	var san santri.Santri

	ses := s.Copy()
	defer ses.Close()

	c := ses.DB(konst.DBName).C(konst.DBUstaz)

	if status == 0 {
		err := c.Find(bson.M{"username": username}).One(&ustaz)
		if err != nil {
			return false, "Data Ustaz Tidak Ditemukan"
		}
		returnUstaz := ustaz.UstazNotSelf()
		jsonUstaz, _ := json.Marshal(returnUstaz)
		return true, string(jsonUstaz)
	}

	err := c.Find(bson.M{"username": username}).One(&ustaz)
	if err != nil {
		return false, "Data Ustaz Tidak Ditemukan"
	}
	returnUstaz := make(map[string]interface{})
	returnUstaz["username"] = ustaz.GetUsername()
	returnUstaz["nama"] = ustaz.GetNama()
	returnUstaz["noid"] = ustaz.GetNoId()
	returnUstaz["jenisid"] = ustaz.GetJenisId()
	returnUstaz["jk"] = ustaz.GetJK()
	returnUstaz["email"] = ustaz.GetEmail()
	returnUstaz["nohp"] = ustaz.GetNoHP()
	returnUstaz["riwayatpendidikan"] = ustaz.GetAllRiwayatPendidikan()
	returnUstaz["fotoprofil"] = ustaz.GetFotoProfil()
	var returnSantri []santrii
	santri := ustaz.GetUsernameSantri()
	d := ses.DB(konst.DBName).C(konst.DBSantri)
	for i := 0; i < len(santri); i++ {
		fmt.Println(santri[i])
		err = d.Find(bson.M{"username": santri[i]}).One(&san)
		// if err != nil {
		// 	return false, "Data Santri di Ustaz Tidak Ditemukan"
		// }
		santriii := make(map[string]interface{})
		santriii["username"] = san.GetUsername()
		santriii["nama"] = san.GetNama()
		santriii["fotoprofil"] = san.GetFotoProfil()
		santriii["jk"] = san.GetJK()
		santriii["email"] = san.GetEmail()
		santriii["nohp"] = san.GetNoHP()
		fmt.Println(santriii)
		returnSantri = append(returnSantri, santriii)
		fmt.Println(returnSantri)
	}
	returnUstaz["santri"] = returnSantri
	jsonUstaz, _ := json.Marshal(returnUstaz)
	return true, string(jsonUstaz)
}


