package konst

import (
	"fmt"
	"net/http"
)

const DBName = "musyrif"
const DBUser = "user"
const DBSantri = "santri"
const DBUstaz = "ustaz"
const DBHafalan = "hafalan"
const DBAlquranImage = "imgalquran"

const HeaderToken = "Auth"
const HeaderSession = "Session"

const KeyDES = "musyrifmtqmnxvmalangubum"

const HeaderJWT = `{"alg":"HS256", "typ":"JWT"}`

const PathHafalan = "audio/hafalan/"
const PathAlquran = "images/alquran/"
const PathPPUstaz = "images/fotoprofil/ustaz/"
const PathPPSantri = "images/fotoprofil/santri/"

var IndexSurah map[int]string
var SurahIndex map[string]int

/*Standar json pengembalian jika mengalami error*/
func ErrorReturn(w http.ResponseWriter, pesan string, code int) string {
	w.WriteHeader(code)
	return fmt.Sprintf("{\"code\": %d, \"message\": \"%s\"}", code, pesan)
}

/*Standar json pengembalian jika berhasil*/
func SuccessReturn(w http.ResponseWriter, pesan string, code int) string {
	w.WriteHeader(code)
	return fmt.Sprintf("{\"code\": %d, \"message\": \"%s\"}", code, pesan)
}

func CreateSurahIndex() {
	surah := []string{"Annaba", "Annaziat", "Abasa", "Attakwir", "Alinfithar", "Almuthaffifin", "Alinsyiqaq", "Alburuj", "Aththariq",
		"Alala", "Alghasyiyah", "Alfajr", "Albalad", "Asysyams", "Allail", "Adhdhuha", "Alinsyirah", "Attiin", "Alalaq", "Alqadr",
		"Albayyinah", "Azzalzalah", "Aladiyat", "Alqariah", "Attakatsur", "Alashr", "Alhumazah", "Alfiil", "Quraisy", "Almaun", "Alkautsar",
		"Alkafirun", "Annashr", "Allahab", "Alikhlas", "Alfalaq", "Annaas"}

	for i := 0; i < len(surah); i++ {
		IndexSurah[78+i] = surah[i]
		SurahIndex[surah[i]] = 78 + i
	}
}

func GetIndex(surah string) int {
	return SurahIndex[surah]
}

func GetSurah(index int) string {
	return IndexSurah[index]
}

func GetRoleString(role int) (bool, string) {
	/* Role
	   1. Santri
	   2. Ustaz/ah
	*/
	if role == 1 {
		return true, "santri"
	} else if role == 2 {
		return true, "ustaz"
	}

	return false, "Role Tidak Dikenal"
}
