package correction

import "gopkg.in/mgo.v2/bson"

type Coordinate struct {
	NomorUrut  int     `json:"nomorurut,omitempty" bson:"nomorurut,omitempty"`
	KoordinatX float64 `json:"koordinatx,omitempty" bson:"koordinatx,omitempty"`
	KoordinatY float64 `json:"koordinaty,omitempty" bson:"koordinaty,omitempty"`
}

type Correction struct {
	Id          bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	Koordinat   []Coordinate  `json:"koordinat,omitempty" bson:"koordinat,omitempty"`
	TeksKoreksi string        `json:"tekskoreksi,omitempty" bson:"tekskoreksi,omitempty"`
}

func (c *Correction) SetId(id string) {
	c.Id = bson.ObjectIdHex(id)
}

func (c *Correction) GetKoordinat() []Coordinate {
	return c.Koordinat
}

func (c *Correction) GetTeksKoreksi() string {
	return c.TeksKoreksi
}

func (c *Correction) SetKoordinat(koordinat []Coordinate) {
	c.Koordinat = koordinat
}

func (c *Correction) SetTeksKoreksi(tekskoreksi string) {
	c.TeksKoreksi = tekskoreksi
}

//Getter dan Setter untuk Coordinate
func (c *Coordinate) GetNomorUrut() int {
	return c.NomorUrut
}

func (c *Coordinate) GetKoordinatX() float64 {
	return c.KoordinatX
}

func (c *Coordinate) GetKoordinatY() float64 {
	return c.KoordinatY
}

func (c *Coordinate) SetNomorUrut(nomorurut int) {
	c.NomorUrut = nomorurut
}

func (c *Coordinate) SetKoordinatX(koordinatx float64) {
	c.KoordinatX = koordinatx
}

func (c *Coordinate) SetKoordinatY(koordinaty float64) {
	c.KoordinatY = koordinaty
}
