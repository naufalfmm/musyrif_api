package enkripsi

import (
	"crypto/cipher"
	"crypto/des"
	"encoding/hex"

	"fmt"

	"../../konst"
)

type Encry struct {
	MessageText []byte
	EncryptText string
}

func NormalizedMessage(msg []byte) []byte {
	if len(msg)%8 == 0 {
		return msg
	}
	padding := (8 * (int(len(msg)/8) + 1)) - len(msg)
	for i := 0; i < padding; i++ {
		msg = append(msg, byte(padding))
	}

	return msg
}

func (e *Encry) GetEncryptText() string {
	return e.EncryptText
}

func (e *Encry) SetEncryptText(encrypttext string) {
	e.EncryptText = encrypttext
}

func (e *Encry) SetMessageText(msgtext []byte) {
	e.MessageText = msgtext
}

func (e *Encry) GetMessageText() []byte {
	return e.MessageText
}

func KeyTripleDES(key string) string {
	if len(key) == 24 {
		return key
	}

	if len(key) > des.BlockSize {
		return key[:des.BlockSize] + key[:des.BlockSize] + key[:des.BlockSize]
	} else {
		padding := key[:(des.BlockSize - len(key))]
		return key + padding + key + padding + key + padding
	}
}

func (e *Encry) DESEncryption() {
	key := KeyTripleDES(konst.KeyDES)
	block, _ := des.NewTripleDESCipher([]byte(key))
	iv := []byte("cobasaja")
	mode := cipher.NewCBCEncrypter(block, iv)

	mes := NormalizedMessage(e.GetMessageText())
	encrypbyte := make([]byte, len(mes))
	mode.CryptBlocks(encrypbyte, mes)
	encryptext := fmt.Sprintf("%x", encrypbyte)
	e.SetEncryptText(encryptext)
}

func (e *Encry) DESDescryption() {
	cip, _ := hex.DecodeString(e.GetEncryptText())
	key := KeyTripleDES(konst.KeyDES)
	block, _ := des.NewTripleDESCipher([]byte(key))
	iv := []byte("cobasaja")
	mode := cipher.NewCBCDecrypter(block, iv)
	textbyte := make([]byte, len(cip))
	mode.CryptBlocks(textbyte, cip)
	e.SetEncryptText(string(textbyte))
}
