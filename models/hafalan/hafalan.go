package hafalan

import (
	"../correction"
	"gopkg.in/mgo.v2/bson"
)

type Hafalan struct {
	Id             bson.ObjectId         `json:"id,omitempty" bson:"_id,omitempty"`
	UsernameUstaz  string                `json:"usernameustaz,omitempty" bson:"usernameustaz,omitempty"`
	UsernameSantri string                `json:"usernamesantri,omitempty" bson:"usernamesantri,omitempty"`
	Koreksi        correction.Correction `json:"koreksi,omitempty" bson:"koreksi,omitempty"`
	NamaHafalan    string                `json:"namahafalan,omitempty" bson:"namahafalan,omitempty"` //nama filenya aja
	NamaGambar     []string              `json:"namagambar,omitempty" bson:"namagambar,omitempty"`   //nama file halaman Alquran
}

func (h *Hafalan) CreateHafalan(dataHafalan map[string]interface{}) {
	h.SetId(dataHafalan["id"].(string))
	h.UsernameUstaz = dataHafalan["usernameustaz"].(string)
	h.UsernameSantri = dataHafalan["usernamesantri"].(string)
	// data, chk := dataHafalan["idkoreksi"]
	// if chk {
	// 	h.IdKoreksi = data.(string)
	// }
	h.NamaHafalan = dataHafalan["namahafalan"].(string)
	h.NamaGambar = dataHafalan["namagambar"].([]string)
}

func (h *Hafalan) SetId(id string) {
	h.Id = bson.ObjectIdHex(id)
}

func (h *Hafalan) GetId() string {
	return h.Id.Hex()
}

func (h *Hafalan) GetUsernameUstaz() string {
	return h.UsernameUstaz
}

func (h *Hafalan) SetUsernameUstaz(username string) {
	h.UsernameUstaz = username
}

func (h *Hafalan) GetUsernameSantri() string {
	return h.UsernameSantri
}

func (h *Hafalan) SetUsernameSantri(username string) {
	h.UsernameSantri = username
}

func (h *Hafalan) GeKoreksi() correction.Correction {
	return h.Koreksi
}

func (h *Hafalan) SetKoreksi(koreksi correction.Correction) {
	h.Koreksi = koreksi
}

func (h *Hafalan) GetNamaHafalan() string {
	return h.NamaHafalan
}

func (h *Hafalan) SetNamaHafalan(namahafalan string) {
	h.NamaHafalan = namahafalan
}

func (h *Hafalan) GetNamaGambar() []string {
	return h.NamaGambar
}

func (h *Hafalan) SetNamaGambar(namagambar []string) {
	h.NamaGambar = namagambar
}
