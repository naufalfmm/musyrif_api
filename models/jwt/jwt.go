package jwt

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"strings"

	"fmt"

	"../../konst"
)

type JWTMusyrif struct {
	message string
	token   string
}

func (j *JWTMusyrif) GetMessage() string {
	return j.message
}

func (j *JWTMusyrif) SetMessage(msg string) {
	j.message = msg
}

func (j *JWTMusyrif) GetToken() string {
	return j.token
}

func (j *JWTMusyrif) SetToken(token string) {
	j.token = token
}

func StringToBase64(mes string) string {
	mess := []byte(mes)
	return base64.StdEncoding.EncodeToString(mess)
}

func Base64ToString(bes64 string) string {
	mess, _ := base64.StdEncoding.DecodeString(bes64)
	// fmt.Println(hex.EncodeToString(mess))
	return string(mess)
}

func ComputeHMAC256(mes string, key string) string {
	//fmt.Println("ComputeHMAC256")
	//fmt.Println(mes)
	kun := []byte(key)
	pes := []byte(mes)
	h := hmac.New(sha256.New, kun)
	h.Write(pes)
	return base64.RawStdEncoding.EncodeToString(h.Sum(nil))
}

//message dalam bentuk json
func (j *JWTMusyrif) TokenMaker() {
	fmt.Println("TokenMaker")
	header := konst.HeaderJWT
	message := j.GetMessage()
	fmt.Printf("message:%s\n", message)
	a := StringToBase64(header) + "." + StringToBase64(message)
	sign := ComputeHMAC256(a, konst.KeyDES)
	tokenn := a + "." + sign
	j.token = tokenn
}

func (j *JWTMusyrif) CheckTokenValidity() (bool, string) {
	token := j.GetToken()
	breakToken := strings.Split(token, ".")
	if len(breakToken) < 2 {
		return false, "Token Tidak Valid"
	}
	if breakToken[0] == "" || breakToken[1] == "" || breakToken[2] == "" {
		return false, "Token Tidak Valid"
	}
	signSend := breakToken[2]
	signReal := ComputeHMAC256(breakToken[0]+"."+breakToken[1], konst.KeyDES)
	if stat := hmac.Equal([]byte(signSend), []byte(signReal)); !stat {
		return false, "Token Tidak Valid"
	}
	return true, "Token Valid"
}

func (j *JWTMusyrif) GetMessageFromToken() {
	token := j.GetToken()
	fmt.Println(token)
	breakToken := strings.Split(token, ".")
	j.SetMessage(Base64ToString(breakToken[1]))
}
