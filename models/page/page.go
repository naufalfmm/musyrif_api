package page

import (
	"encoding/hex"

	"../../konst"
)

type Page struct {
	Id         string `json:"id,omitempty" bson:"_id,omitempty"`
	Halaman    int    `json:"halaman,omitempty" bson:"halaman,omitempty"`
	NamaSurat  int    `json:"namasurat,omitempty" bson:"namasurat,omitempty"`
	AyatAwal   int    `json:"ayatawal,omitempty" bson:"ayatawal,omitempty"`
	AyatAkhir  int    `json:"ayatakhir,omitempty" bson:"ayatakhir,omitempty"`
	NamaGambar string `json:"namagambar,omitempty" bson:"namagambar,omitempty"`
}

func (p *Page) GetId() string {
	return hex.EncodeToString([]byte(p.Id))
}

func (p *Page) GetNamaSuratString() string {
	return konst.GetSurah(p.NamaSurat)
}

func (p *Page) GetNamaSuratInt() int {
	return p.NamaSurat
}

func (p *Page) GetHalaman() int {
	return p.Halaman
}

func (p *Page) SetHalaman(halaman int) {
	p.Halaman = halaman
}

func (p *Page) SetNamaSuratString(surah string) {
	p.NamaSurat = konst.GetIndex(surah)
}

func (p *Page) SetNamaSuratInt(index int) {
	p.NamaSurat = index
}

func (p *Page) GetAyatAwal() int {
	return p.AyatAwal
}

func (p *Page) SetAyatAwal(ayat int) {
	p.AyatAwal = ayat
}

func (p *Page) GetAyatAkhir() int {
	return p.AyatAkhir
}

func (p *Page) SetAyatAkhir(ayat int) {
	p.AyatAkhir = ayat
}

func (p *Page) GetNamaGambar() string {
	return p.NamaGambar
}

func (p *Page) SetNamaGambar(namagambar string) {
	p.NamaGambar = namagambar
}
