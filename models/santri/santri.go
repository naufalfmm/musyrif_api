package santri

import (
	"encoding/hex"
	"fmt"

	"../../konst"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Santri struct {
	Id       string `json:"id,omitempty" bson:"_id,omitempty"`
	Username string `json:"username,omitempty" bson:"username,omitempty"`
	Password string `json:"password,omitempty" bson:"password,omitempty"`
	// Password_Again string `json:"password_again,omitempty" bson:"password_again,omitempty"`
	Nama          string `json:"nama,omitempty" bson:"nama,omitempty"`
	NoID          string `json:"noid,omitempty" bson:"noid,omitempty"`
	JenisID       int    `json:"jenisid,omitempty" bson:"jenisid,omitempty"` //1: KTP, 2: SIM, 3: Paspor
	JK            string `json:"jk,omitempty" bson:"jk,omitempty"`
	Email         string `json:"email,omitempty" bson:"email,omitempty"`
	NoHP          string `json:"nohp,omitempty" bson:"nohp,omitempty"`
	FotoProfil    string `json:"fotoprofil,omitempty" bson:"fotoprofil,omitempty"`
	UsernameUstaz string `json:"usernameustaz,omitempty" bson:"usernameustaz,omitempty"`
}

func (s *Santri) CreateSantriReg(dataSantri map[string]interface{}) {
	s.Username = dataSantri["username"].(string)
	s.Password = dataSantri["password"].(string)
	s.Nama = dataSantri["nama"].(string)
	s.NoID = dataSantri["noid"].(string)
	s.JenisID = int(dataSantri["jenisid"].(float64))
	s.JK = dataSantri["jk"].(string)
	s.Email = dataSantri["email"].(string)
	s.NoHP = dataSantri["nohp"].(string)
	dataa, stat := dataSantri["fotoprofil"]
	if stat {
		s.FotoProfil = dataa.(string)
	}
	data, ch := dataSantri["usernameustaz"].(string)
	if ch {
		s.UsernameUstaz = data
	}
	fmt.Println(s)
}

//Mengembalikan data ustaz untuk user yang bukan miliknya
func (s *Santri) SantriNotSelf() map[string]interface{} {
	returnSantri := make(map[string]interface{})
	returnSantri["username"] = s.GetUsername()
	returnSantri["nama"] = s.GetNama()
	returnSantri["jk"] = s.GetJK()
	return returnSantri
}

func (s *Santri) SetFotoProfil(fotoprofil string) {
	s.FotoProfil = fotoprofil
}

func (s *Santri) GetFotoProfil() string {
	return s.FotoProfil
}

func (s *Santri) SetId(id string) {
	s.Id = id
}

func (s *Santri) GetId() string {
	return hex.EncodeToString([]byte(s.Id))
}

func (s *Santri) GetUsername() string {
	return s.Username
}

func (s *Santri) SetUsername(username string) {
	s.Username = username
}

func (s *Santri) GetPassword() string {
	return s.Password
}

func (s *Santri) SetPassword(password string) {
	s.Password = password
}

// func (s Santri) GetPasswordAgain() string {
// 	return s.Password_Again
// }

// func (s Santri) SetPasswordAgain(passwordagain string) {
// 	s.Password_Again = passwordagain
// }

func (s *Santri) GetNama() string {
	return s.Nama
}

func (s *Santri) SetNama(nama string) {
	s.Nama = nama
}

func (s *Santri) GetNoId() string {
	return s.NoID
}

func (s *Santri) SetNoId(noid string) {
	s.NoID = noid
}

func (s *Santri) GetJenisId() int {
	return s.JenisID
}

func (s *Santri) SetJenisId(jenisid int) {
	s.JenisID = jenisid
}

func (s *Santri) GetJK() string {
	return s.JK
}

func (s *Santri) SetJK(jk string) {
	s.JK = jk
}

func (s *Santri) GetEmail() string {
	return s.Email
}

func (s *Santri) SetEmail(email string) {
	s.Email = email
}

func (s *Santri) GetNoHP() string {
	return s.NoHP
}

func (s *Santri) SetNoHP(nohp string) {
	s.NoHP = nohp
}

func (s *Santri) GetUsernameUstaz() string {
	return s.UsernameUstaz
}

func (s *Santri) SetUsernameUstaz(usernameustaz string) {
	s.UsernameUstaz = usernameustaz
}

func (s *Santri) DeleteUsernameUstaz(ss *mgo.Session) bool {
	ses := ss.Copy()
	defer ses.Close()

	s.SetUsernameUstaz("")

	c := ses.DB(konst.DBName).C(konst.DBSantri)

	err := c.Update(bson.M{"username": s.GetUsername()}, s)
	if err != nil {
		return false
	}
	return true
}
