package session

import (
	"sync"
	"time"
)

type Client struct {
	IdClient string
	Username string
	Role     string
}

type Session struct {
	SessionStore map[string]int64
	StorageMutex sync.RWMutex
}

func (c *Client) CreateClient(id string, username string, role string) {
	c.IdClient = id
	c.Username = username
	c.Role = role
}

func (c *Client) SetIdClient(id string) {
	c.IdClient = id
}

func (c *Client) SetUsernameClient(username string) {
	c.Username = username
}

func (c *Client) SetRoleClient(role string) {
	c.Role = role
}

func (s *Session) StoreSession(session string) {
	s.StorageMutex.Lock()
	s.SessionStore[session] = time.Now().Unix()
	s.StorageMutex.Unlock()
}

func (s *Session) DeleteSession(session string) {
	s.StorageMutex.Lock()
	delete(s.SessionStore, session)
	s.StorageMutex.Unlock()
}

func (s *Session) CheckSessionExist(sessid string) bool {
	s.StorageMutex.Lock()
	_, stat := s.SessionStore[sessid]
	s.StorageMutex.Unlock()
	return stat
}

func (s *Session) ExpiredSession() {
	s.StorageMutex.Lock()
	for key, value := range s.SessionStore {
		if value <= time.Now().Unix() {
			delete(s.SessionStore, key)
		}
	}
	s.StorageMutex.Unlock()
}
