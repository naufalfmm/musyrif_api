package user

import (
	"gopkg.in/mgo.v2/bson"

	"../../konst"
)

type User struct {
	Id       bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	Username string        `json:"username,omitempty" bson:"username,omitempty"`
	Password string        `json:"password,omitempty" bson:"password,omitempty"`
	// Password_Again string `json:"password_again,omitempty" bson:"password_again,omitempty"`
	LoginType int `json:"logintype,omitempty" bson:"logintype,omitempty"` //1: santri, 2: ustaz/ustazah
}

//Digunakan untuk Registrasi data User
func (u *User) CreateUserReg(data map[string]interface{}) {
	u.Id = bson.ObjectIdHex(data["id"].(string))
	u.Username = data["username"].(string)
	u.Password = data["password"].(string)
	u.LoginType = int(data["role"].(float64))
}

func (u *User) GetId() string {
	return u.Id.Hex()
}

func (u *User) GetUsername() string {
	return u.Username
}

func (u *User) GetPassword() string {
	return u.Password
}

func (u *User) GetLoginType() (bool, string) {
	stat, msg := konst.GetRoleString(u.LoginType)
	return stat, msg
}

func (u *User) GetLoginTypeInt() int {
	return u.LoginType
}
