package ustaz

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"../../konst"
)

type Ustaz struct {
	Id       string `json:"id,omitempty" bson:"_id,omitempty"`
	Username string `json:"username,omitempty" bson:"username,omitempty"`
	Password string `json:"password,omitempty" bson:"password,omitempty"`
	// Password_Again    string   `json:"password_again,omitempty" bson:"password_again,omitempty"`
	Nama              string   `json:"nama,omitempty" bson:"nama,omitempty"`
	NoID              string   `json:"noid,omitempty" bson:"noid,omitempty"`
	JenisID           int      `json:"jenisid,omitempty" bson:"jenisid,omitempty"` //1: KTP, 2: SIM, 3: Paspor
	JK                string   `json:"jk,omitempty" bson:"jk,omitempty"`
	Email             string   `json:"email,omitempty" bson:"email,omitempty"`
	NoHP              string   `json:"nohp,omitempty" bson:"nohp,omitempty"`
	FotoProfil        string   `json:"fotoprofil,omitempty" bson:"fotoprofil,omitempty"`
	RiwayatPendidikan []string `json:"riwayatpendidikan,omitempty" bson:"riwayatpendidikan,omitempty"`
	UsernameSantri    []string `json:"usernamesantri,omitempty" bson:"usernamesantri,omitempty"`
}

func (u *Ustaz) CreateUstazReg(dataUstaz map[string]interface{}) {
	u.Username = dataUstaz["username"].(string)
	u.Password = dataUstaz["password"].(string)
	u.Nama = dataUstaz["nama"].(string)
	u.NoID = dataUstaz["noid"].(string)
	u.JenisID = dataUstaz["jenisid"].(int)
	u.JK = dataUstaz["jk"].(string)
	u.Email = dataUstaz["email"].(string)
	u.NoHP = dataUstaz["nohp"].(string)
	data, stat := dataUstaz["fotoprofil"]
	if stat {
		u.FotoProfil = data.(string)
	}
}

//Mengembalikan data ustaz untuk user yang bukan miliknya
func (u *Ustaz) UstazNotSelf() map[string]interface{} {
	returnUstaz := make(map[string]interface{})
	returnUstaz["username"] = u.GetUsername()
	returnUstaz["nama"] = u.GetNama()
	returnUstaz["jk"] = u.GetJK()
	return returnUstaz
}

func (u *Ustaz) SetFotoProfil(fotoprofil string) {
	u.FotoProfil = fotoprofil
}

func (u *Ustaz) GetFotoProfil() string {
	return u.FotoProfil
}

func (u *Ustaz) SetId(id string) {
	u.Id = id
}

func (u *Ustaz) GetId() string {
	return hex.EncodeToString([]byte(u.Id))
}

func (u *Ustaz) GetUsername() string {
	return u.Username
}

func (u *Ustaz) SetUsername(username string) {
	u.Username = username
}

func (u *Ustaz) GetPassword() string {
	return u.Password
}

func (u *Ustaz) SetPassword(password string) {
	encryptPass := sha256.Sum256([]byte(password))
	u.Password = fmt.Sprintf("%x", encryptPass)
}

// func (u Ustaz) GetPasswordAgain() string {
// 	return u.Password_Again
// }

// func (u Ustaz) SetPasswordAgain(passwordagain string) {
// 	u.Password_Again = passwordagain
// }

func (u *Ustaz) GetNama() string {
	return u.Nama
}

func (u *Ustaz) SetNama(nama string) {
	u.Nama = nama
}

func (u *Ustaz) GetNoId() string {
	return u.NoID
}

func (u *Ustaz) SetNoId(noid string) {
	u.NoID = noid
}

func (u *Ustaz) GetJenisId() int {
	return u.JenisID
}

func (u *Ustaz) SetJenisId(jenisid int) {
	u.JenisID = jenisid
}

func (u *Ustaz) GetJK() string {
	return u.JK
}

func (u *Ustaz) SetJK(jk string) {
	u.JK = jk
}

func (u *Ustaz) GetEmail() string {
	return u.Email
}

func (u *Ustaz) SetEmail(email string) {
	u.Email = email
}

func (u *Ustaz) GetNoHP() string {
	return u.NoHP
}

func (u *Ustaz) SetNoHP(nohp string) {
	u.NoHP = nohp
}

func (u *Ustaz) GetUsernameSantri() []string {
	return u.UsernameSantri
}

func (u *Ustaz) SetUsernameSantri(usernamesantri []string) {
	u.UsernameSantri = usernamesantri
}

func (u *Ustaz) DeleteUsernameSantri(s *mgo.Session, usernamesantri string) bool {
	dataSantri := u.GetUsernameSantri()

	ses := s.Copy()
	defer ses.Close()

	for i := 0; i < len(dataSantri); i++ {
		if dataSantri[i] == usernamesantri {
			dataSantri[i] = ""
		}
	}

	c := ses.DB(konst.DBName).C(konst.DBUstaz)

	err := c.Update(bson.M{"username": u.GetUsername()}, u)
	if err != nil {
		return false
	}
	return true
}

//Get Riwayat Pendidikan
func (u *Ustaz) GetAllRiwayatPendidikan() []string {
	return u.RiwayatPendidikan
}
