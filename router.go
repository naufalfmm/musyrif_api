package main

import (
	"fmt"
	"io"
	"log"
	"net/http"

	"./controller/correction"
	"./controller/hafalan"
	"./controller/page"
	"./controller/user"
	"./models/session"

	"./konst"

	"github.com/rs/cors"
)

type UserHandler struct {
	session *session.Session
}
type RecitationHandler struct {
	session *session.Session
}
type AlquranHandler struct {
	session *session.Session
}
type CorrectionHandler struct {
	session *session.Session
}

func (u UserHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, users.UserController(res, req, req.RequestURI, u.session))
}

func (r RecitationHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, hafalann.HafalanController(res, req, req.RequestURI, r.session))
}

func (c CorrectionHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, correctionn.CorrectionController(res, req, req.RequestURI, c.session))
}

func (a AlquranHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, pagee.PageController(res, req, req.RequestURI, a.session))
}

func DefaultServe(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(http.StatusNotFound)
	io.WriteString(res, fmt.Sprintf("{\"error\": %d, \"message\":\"%s\"}", http.StatusNotFound, "Path Tidak Ditemukan"))
}

func HelloServe(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(http.StatusOK)
	io.WriteString(res, fmt.Sprintf("{\"error\": %d, \"message\":\"%s\"}", http.StatusOK, "Halo Pengunjung"))
}

func main() {
	var sesi session.Session
	sesi.SessionStore = make(map[string]int64)
	var user = UserHandler{&sesi}
	var recite = RecitationHandler{&sesi}
	var correct = CorrectionHandler{&sesi}
	var page = AlquranHandler{&sesi}

	konst.IndexSurah = make(map[int]string)
	konst.SurahIndex = make(map[string]int)

	konst.CreateSurahIndex()

	go sesi.ExpiredSession()

	mux := http.NewServeMux()
	mux.Handle("/user/", user)
	mux.Handle("/login/", user)
	mux.Handle("/logout/", user)
	mux.Handle("/registrasi/", user)
	mux.Handle("/hafalan/", recite)
	mux.Handle("/koreksi/", correct)
	mux.Handle("/alquran/", page)
	mux.Handle("/ustaz/", user)
	mux.HandleFunc("/hello/", HelloServe)
	mux.HandleFunc("/", DefaultServe)

	handler := cors.Default().Handler(mux)
	log.Fatal(http.ListenAndServe(":10317", handler))
}
